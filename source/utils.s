.arm
.align 4
.code 32
.text

.global enable_vram_rw
.type enable_vram_rw, %function
enable_vram_rw:
	@ Drain write buffer
	mcr p15, 0, r0, c7, c10, 4

	@ Map VRAM to region 7
	@ Region base: 0x18000000
	@ Region size: 8MB (0b10110)
	ldr r0, =0x1800002D
	mcr p15, 0, r0, c6, c7, 0

	@ Set region 7 permissions:
	@ Privileged: Read/write access
	@ User: Read/write access
	mrc p15, 0, r0, c5, c0, 2
	bic r0, r0, #(0b1111 << 28)
	orr r0, r0, #(0b0011 << 28)
	mcr p15, 0, r0, c5, c0, 2

	bx lr

.global remove_MPU_perm
.type remove_MPU_perm, %function
remove_MPU_perm:
	mrc p15, 0, r0, c5, c0, 2
	ldr r0, =0x33333333       @ all regions, user/priv: r/w
	mcr p15, 0, r0, c5, c0, 2
	bx lr

.global FlushDataCache
.type FlushDataCache, %function
FlushDataCache:
	mov r0, #0
	mcr p15, 0, r0, c7, c6, 0
	bx lr

.global DrainWriteBuffer
.type DrainWriteBuffer, %function
DrainWriteBuffer:
	mov r0, #0
	mcr p15, 0, r0, c7, c10, 4
	bx lr

.global disable_IRQ
.type disable_IRQ, %function
disable_IRQ:
	mrs r0, cpsr
	orr r0, r0, #0x80  @IRQ
	msr cpsr_c, r0
	bx lr

.global enable_IRQ
.type enable_IRQ, %function
enable_IRQ:
	mrs r0, cpsr
	bic r0, r0, #0x80  @IRQ
	msr cpsr_c, r0
	bx lr

.global disable_FIQ
.type disable_FIQ, %function
disable_FIQ:
	mrs r0, cpsr
	orr r0, r0, #0x40  @FIQ
	msr cpsr_c, r0
	bx lr

.global enable_FIQ
.type enable_FIQ, %function
enable_FIQ:
	mrs r0, cpsr
	bic r0, r0, #0x40  @FIQ
	msr cpsr_c, r0
	bx lr

.global disable_MPU
.type disable_MPU, %function
disable_MPU:
	mrc p15, 0, r0, c1, c0, 0
	bic r0, r0, #1
	mcr p15, 0, r0, c1, c0, 0
	bx lr

.global enable_MPU
.type enable_MPU, %function
enable_MPU:
	mrc p15, 0, r0, c1, c0, 0
	orr r0, r0, #1
	mcr p15, 0, r0, c1, c0, 0
	bx lr
