#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include "common.h"
#include "draw.h"
#include "fs.h"
#include "hid.h"
#include "i2c.h"
#include "config.h"

void reboot();

extern void *arm11_map_fb_start;
extern void *arm11_map_fb_end;
extern void *arm11_stage0_start;
extern void *arm11_stage0_end;
extern void *arm11_stage1_start;
extern void *arm11_stage1_end;


// Linux console
static int shared_block = 1;
static unsigned int cur_color = WHITE;

#define KERN_SOH_ASCII  '\001'
static unsigned int get_kern_level_color(char kern)
{
	switch (kern) {
	case '0':  // KERN_EMERG
		return RED;
	case '1': // KERN_ALERT
		return CYAN;
	case '2': // KERN_CRIT
		return ORANGE;
	case '3': // KERN_ERR
		return PINK;
	case '4': // KERN_WARNING
		return YELLOW;
	case '5': // KERN_NOTICE
		return LIGHT_GREEN;
	case '6': // KERN_INFO
		return GREEN;
	case '7': // KERN_DEBUG
		return GREY;
	case 'd': // KERN_DEFAULT
	default:
		return WHITE;
	}
}

static char shared_getc()
{
	register char c;

	while ((c = SHARED_CHAR) == 0) {
		if (BUTTON_PRESS(BUTTON_START)) {
			reboot();
		}
	}

	do {
		if (BUTTON_PRESS(BUTTON_START)) {
			reboot();
		} else if (BUTTON_PRESS(BUTTON_R1)) {
			shared_block = 1;
		} else if (BUTTON_PRESS(BUTTON_L1)) {
			shared_block = 0;
		}
	} while (!BUTTON_PRESS(BUTTON_Y) && shared_block);

	SHARED_CHAR = 0;

	return c;
}

void arm11_map_fb()
{
	// Copy the payload...
	const unsigned int arm11_map_fb_size = (u32)&arm11_map_fb_end - (u32)&arm11_map_fb_start;
	memcpy((void *)ARM11_MAP_FB_ADDR, &arm11_map_fb_start, arm11_map_fb_size);
	// Make ARM11 jump to the MAP FB payload
	*(unsigned int *)0x1FFFFFF8 = ARM11_MAP_FB_ADDR;
}

int load_linux()
{
	// Load the Linux image
	DEBUG(SCREEN_TOP, "Opening \"" LINUXIMAGE_FILENAME "\"...\n");

	if (!FileOpen(LINUXIMAGE_FILENAME)) {
		DEBUG(SCREEN_TOP, "Error opening " LINUXIMAGE_FILENAME);
		return -1;
	}

	size_t offset = 0, bytes_read = 0;
	while ((bytes_read = FileRead((void *)(ZIMAGE_ADDR + offset), 0x10000, offset)) > 0) {
		offset += bytes_read;
	}

	DEBUG(SCREEN_TOP, "Loaded " LINUXIMAGE_FILENAME ":\n");
	DEBUG(SCREEN_TOP, "    address: %p\n", ZIMAGE_ADDR);
	DEBUG(SCREEN_TOP, "    size:    0x%08X bytes\n", offset);

	//Load the device tree to PARAMS_ADDR
	DEBUG(SCREEN_TOP, "Opening \"" DTB_FILENAME "\"...\n");

	if (!FileOpen(DTB_FILENAME)) {
		DEBUG(SCREEN_TOP, "Error opening " DTB_FILENAME);
		return -1;
	}

	offset = 0, bytes_read = 0;
	while ((bytes_read = FileRead((void*)(PARAMS_ADDR + offset), 0x1000, offset)) > 0) {
		offset += bytes_read;
	}

	DEBUG(SCREEN_TOP, "Loaded " DTB_FILENAME ":\n");
	DEBUG(SCREEN_TOP, "    address: %p\n", PARAMS_ADDR);
	DEBUG(SCREEN_TOP, "    size:    0x%08X bytes\n", offset);

	DEBUG(SCREEN_TOP, "Copying ARM11 payloads...\n");

	const unsigned int arm11_stage0_size = (u32)&arm11_stage0_end - (u32)&arm11_stage0_start;
	const unsigned int arm11_stage1_size = (u32)&arm11_stage1_end - (u32)&arm11_stage1_start;

	memcpy((void *)ARM11_STAGE0_ADDR, &arm11_stage0_start, arm11_stage0_size);
	memcpy((void *)ARM11_STAGE1_ADDR, &arm11_stage1_start, arm11_stage1_size);

	DEBUG(SCREEN_TOP, "ARM11 Stage0 size: %d bytes\n", arm11_stage0_size);
	DEBUG(SCREEN_TOP, "ARM11 Stage1 size: %d bytes\n", arm11_stage1_size);

	SHARED_CHAR = 0;
	ClearScreen(SCREEN_TOP | SCREEN_BOT);
	console_set_y(8*2);

	// Make ARM11 jump to arm11_payload
	*(unsigned int *)0x1FFFFFF8 = ARM11_STAGE0_ADDR;

	while (1) {
		register char c = shared_getc();
		if (c == KERN_SOH_ASCII) {
			char level = shared_getc();
			//console_printf(SCREEN_TOP, WHITE, "level: %d\n", level);
			cur_color = get_kern_level_color(level);
		} else {
			console_putc(SCREEN_TOP, cur_color, c);
		}
	}

	return 0;
}

int main()
{
	// First we need to map the FBs to the VRAM
	arm11_map_fb();
	// Enable ARM9 R/W to the VRAM
	enable_vram_rw();

	ClearScreen(SCREEN_TOP | SCREEN_BOT);

	while (true) {
		ClearScreen(SCREEN_TOP);
		console_reset();
		DEBUG(SCREEN_TOP, "ARM11 linux loader by xerpi\n\n");
		DEBUG(SCREEN_TOP, "A: Boot Linux\n");
		DEBUG(SCREEN_TOP, "START: reboot\n");

		u32 pad_state = InputWait();
		if (pad_state & BUTTON_A) {
			InitFS();
			DEBUG(SCREEN_TOP, "Load Linux: %s!", load_linux() == 0 ? "succeeded\n" : "failed\n");
			DEBUG(SCREEN_TOP, "Press any key to continue\n");
			DeinitFS();
			InputWait();
		} else if (pad_state & BUTTON_START) {
			goto reboot;
		}
	}

reboot:
	reboot();
	DeinitFS();
	return 0;
}

void reboot()
{
	i2cWriteRegister(I2C_DEV_MCU, 0x20, 1 << 2);
	while(true);
}
